const Deferred = require('JQDeferred');
const http = require('http');
const express = require('express');
const chokidar = require('chokidar');
const fs = require('fs');
const Log = require('log');
const gulp = require('gulp');
const jade = require('gulp-jade');
const clean = require('gulp-clean');
const insert = require('gulp-insert');
const rename = require('gulp-rename');
const babel = require('gulp-babel');
const coffee = require('gulp-coffee');
const latest = require('babel-preset-latest');
const stage0 = require('babel-preset-stage-0');
const converter = {
	less: require('gulp-less'),
	scss: require('gulp-sass'),
	styl: require('gulp-stylus')
};
const app = express();
const log = new Log('info');

let watchMode = false;
let watcher;
let onError;

const doResponse = (res, conf) => {
	const composeResponse = (toWrite) => {
		res.setHeader('Content-type', 'text/html; charset=utf-8;');
		res.write(toWrite);
		res.end();
	};

	if( conf.filePath ){
		fs.readFile(conf.filePath, (err, content) => {
			if (err) {
				log.error(err.message);
			}

			composeResponse(content);
		});
	}
	else if( conf.status && conf.msg ){
		composeResponse(JSON.stringify(conf));
	}
};

const parsePostData = (data) => {
	let post;

	try{
		post = JSON.parse(data);
	}
	catch (e){
		onError(e);
	}

	if( post && post.domName && post.cmpName && post.styleSrcType && post.contentSrcType && post.scriptSrcType ){
		return post;
	}

	onError({
		status: 'error',
		message: 'Lack of parameters!'
	});

	return false;
};

const extendJadeSource = (src) => {
	let code = 'style\n';

	src.split(',').forEach((type) => {
		code += `\tinclude ${type}.css\n\n`;
	});

	return code;
};

class Process {
	constructor(conf){
		this.conf = conf;
		this.baseUrl = `../${[conf.domName, conf.cmpName, conf.elName].join('/')}/`;
		this.defer = new Deferred();
	}

	run (res) {
		let process = this;

		process.defer.then(() => {
			let typesArr = process.conf.styleSrcType.split(',');

			return Deferred.when(...typesArr.map((type) => {
				let cssDef = new Deferred();

				gulp.src(`${process.baseUrl}${process.conf.varName}/style.${type}`)
					.on('error', onError)
					.pipe(rename((path) => {
						path.basename = type;
					}))
					.pipe(converter[type]())
					.on('error', onError)
					.pipe(insert.transform((contents) => {
						let start = `\n /*${type} start*/ \n`;
						let end = ` /*${type} end*/ \n`;

						return `${start}${contents}${end}`;
					}))
					.pipe(gulp.dest('temp'))
					.on('end', () => {
						log.info(`${type.toUpperCase()} has been converted!`);

						cssDef.resolve();
					});

				return cssDef;
			}));
		}).then(() => {
			let scriptDef = new Deferred();
			let sourceType = process.conf.scriptSrcType;
			let fileSrc = `${process.baseUrl}${process.conf.varName}/script.${sourceType === 'coffee' ? sourceType : 'js'}`;

			gulp.src(fileSrc)
				.on('error', onError)
				.pipe((sourceType === 'es6' ?
						babel({presets: [latest, stage0]}) :
						sourceType === 'coffee' ?
						coffee({bare: true}) : gulp.src(fileSrc)))
				.on('error', onError)
				.pipe(insert.transform((contents) => `\n${contents}\n`))
				.pipe(gulp.dest('temp'))
				.on('end', () => {
					scriptDef.resolve();
				});

			return scriptDef;
		}).then(() => {
			let contentDef = new Deferred();

			gulp.src('index.jade')
				.on('error', onError)
				.pipe(insert.prepend(`|@br\ninclude ../${process.baseUrl}${process.conf.varName}/content.${process.conf.contentSrcType}\n|@br\n`))
				.on('error', onError)
				.pipe(insert.append(`script\n\tinclude script.js\n`))
				.on('error', onError)
				.pipe(gulp.dest('temp'))
				.on('end', () => {
					contentDef.resolve();
				});

			return contentDef;
		}).then(() => {
			let jadeDef = new Deferred();

			gulp.src('temp/index.jade')
				.on('error', onError)
				.pipe(rename((path) => {
					path.basename = process.conf.varName;
				}))
				.pipe(insert.prepend(extendJadeSource(process.conf.styleSrcType)))
				.on('error', onError)
				.pipe(jade())
				.on('error', onError)
				.pipe(insert.transform((contents) => {
					return contents.replace(/@br/g, '\n\n');
				}))
				.on('error', onError)
				.pipe(gulp.dest(process.baseUrl))
				.on('end', () => {
					jadeDef.resolve();
				});

			return jadeDef;
		}).then(() => {
			let msg = `${process.conf.varName}.html assembly is finished!`;

			log.info(msg);
			gulp.src('temp', {read: false}).pipe(clean());

			if( !watchMode ){
				doResponse(res, {
					status: 'ok',
					msg: msg
				});
			}
		}).fail((err) => {
			onError(err);
		});

		process.defer.resolve();
	}
}

app.listen(3000, () => log.info('Server started on 127.0.0.1:3000'));

app.use(express.static('resources'));

app.all('/exit', (req, res) => {
	log.info('Watch mode is disabled!');
	if( watcher && typeof watcher.close === 'function' ){
		watcher.close();
	}
	watchMode = false;
	watcher = undefined;
});

app.all('/', (req, res) => {
	let postData = '';

	const composePostData = (data, path) => {
		let pathArr = path.replace(/\\/g, '/').split('/').reverse();

		data.varName = pathArr[1];
		data.elName = pathArr[2];

		return data;
	};

	onError = (e) => {
		log.error(e.message);
		if( !watchMode ){
			doResponse(res, {
				status: 'error',
				msg: e.message
			});
		}
	};

	if( req.method === 'GET' ){
		doResponse(res, {filePath: 'index.html'});
	}
	else if( req.method === 'POST' ){
		req.on('data', (chunk) => {
			postData += chunk;
		});

		req.on('end', () => {
			let dataObj = parsePostData(postData);

			if( dataObj.watch && !watcher ){
				watcher = chokidar.watch(`../${[dataObj.domName, dataObj.cmpName].join('/')}/*/*/*`, {
					ignored: /[\/\\]\./,
					persistent: true
				});

				watcher.on('change', (path) => {
					if ( path ){
						dataObj = composePostData(dataObj, path);
						new Process(dataObj).run(res);
					}
				});

				watchMode = true;
				log.info('Watch mode is enabled!');
			}
			else{
				new Process(dataObj).run(res);
			}
		});
	}
});