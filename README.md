# PREPROCESSOR #

### What is this repository for? ###

Preprocessor converts formats Less, Sass, Stylus into the single CSS and CoffeeScript or ES6 into the ES5 JS file, and assembles them into the HTML file.

### How do I get set up? ###

* Download the repository and extract the contents into your campaign's working directory (folder with the client domains)

![directory.png](https://bitbucket.org/repo/pEej6p/images/3913701956-directory.png)

* Open WebStorm's Terminal and navigate to the **preprocessor**'s folder (*cd preprocessor*)
* Run *npm install* command to install all NodeJS dependencies (node modules required for proper work)

![terminal.png](https://bitbucket.org/repo/pEej6p/images/1825986645-terminal.png)

### How do I use it? ###

* Create in the Element's directory a folder with the name of the target Variant (*domain.com -> T01_Campaign -> A_Element -> A2_Variant*)
* This folder should contain:

> *style.[extension]* - file can contain Less, Sass or Stylus styles, where [extension] is respective format *less*, *scss* or *styl*

> *content.[html|jade]* - variant's HTML content

> *script.[js|coffee]* - variant's JavaScript scenario

![structure.png](https://bitbucket.org/repo/pEej6p/images/2506117167-structure.png)

* Open WebStorm's Terminal and navigate to the **preprocessor**'s folder (*cd preprocessor*)
* Run *npm start* command to launch application
* Open URL http://127.0.0.1:3000/ in any browser
* Fill in all campaign related fields appropriately (domain name field should looks like *domain.com/Sandbox/1-Debug* if you have *Sandbox* and *1-Debug* folder in structure)
* Choose desired source technologies (at least one must be selected)
* Watch mode (optional) can monitor selected source files and run assembling when a file change is made.
* Click ASSEMBLE button

![ui.png](https://bitbucket.org/repo/pEej6p/images/439662343-ui.png)

* Less than in one second, the application will generate a HTML file with the name of the Variant and will consist of the CSS (converted Less, Sass, Stylus into the CSS) concatenated with the variant's HTML and JS code (content.[ext], script.[ext] files)
* After campaign coding is complete, all source files (*less, scss, styl, coffee, js, jade*) MUST be uploaded to the appropriate [Git repository](https://gitlab.com/groups/maxymiser-emea-media-clients) as well as campaign code.

### Support ###

If you found a bug or has a suggestion how to improve the app, you can contribute via "Give Feedback" button or [THIS LINK](https://gitlab.com/Andy_Zy/preprocessor/issues/new).